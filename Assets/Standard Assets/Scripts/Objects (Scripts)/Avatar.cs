using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class Avatar : Entity
	{
		public string displayName;
		public Transform itemsParent;
		public Item[] items = new Item[0];
		public UseableItem[] useableItems = new UseableItem[0];
		public Transform cooldownIndicatorsParent;
		public Animator animator;
		public bool unlocked;
		public Achievement unlockOnCompleteAchievement;
		public GameObject lockedIndicatorGo;
		public GameObject untriedIndicatorGo;
		public GameObject enabledIndicator;
		public float rotateRateOnPhone;
		public float rotateRateOnComputer;
		public float minCrushAngle;
		public RotateMode rotateMode;
		public static Avatar instance;
		public static Avatar Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Avatar>(true);
				return instance;
			}
		}
		Vector2 previousCursorViewportPoint;
		float rotateRate;
		Dictionary<Collider2D, Collision2D> collisionsDict = new Dictionary<Collider2D, Collision2D>();

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.Awake ();
			if (unlockOnCompleteAchievement == null || unlockOnCompleteAchievement.complete || unlockOnCompleteAchievement.ShouldBeComplete())
			{
				unlocked = true;
				lockedIndicatorGo.SetActive(false);
				untriedIndicatorGo.SetActive(!SaveAndLoadManager.saveData.triedAvatars.Contains(name));
			}
#if UNITY_STANDALONE || UNITY_EDITOR
			rotateRate = rotateRateOnComputer;
#else
			rotateRate = rotateRateOnPhone;
#endif
		}

		public override void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.OnEnable ();
			instance = this;
			items = itemsParent.GetComponentsInChildren<Item>();
			useableItems = itemsParent.GetComponentsInChildren<UseableItem>();
			for (int i = 0; i < items.Length; i ++)
			{
				Item item = items[i];
				if (!item.gameObject.activeSelf)
					continue;
				UseableItem useableItem = item as UseableItem;
				if (useableItem != null)
					useableItem.useAction.performed += useableItem.TryToUse;
				item.OnGain ();
			}
			enabledIndicator.SetActive(true);
		}

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			base.DoUpdate ();
		}

		public override void HandleRotating ()
		{
			if (rotateMode == RotateMode.DeltaX)
				rigid.angularVelocity = InputManager.RotateInput * rotateRate;
			else// if (rotateMode == RotateMode.DeltaAngle)
			{
				Vector2 cursorViewportPoint = (Vector2) GameCamera.Instance.camera.ScreenToViewportPoint((Vector2) InputManager.MousePosition) - Vector2.one / 2;
				rigid.angularVelocity = -Vector2.SignedAngle(cursorViewportPoint, previousCursorViewportPoint) / Time.deltaTime;
				previousCursorViewportPoint = cursorViewportPoint;
			}
		}

		public override void Death ()
		{
			GameMode.instance.End ();
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			enabledIndicator.SetActive(false);
		}

		void OnCollisionEnter2D (Collision2D coll)
		{
			collisionsDict.Remove(coll.collider);
			ContactPoint2D[] contactPoints = new ContactPoint2D[coll.contactCount];
			coll.GetContacts(contactPoints);
			if (contactPoints[0].normal.y > Vector2.one.normalized.x)
			{
				rigid.gravityScale = 0;
				ObjectWithNoGravity objectWithNoGravity = coll.gameObject.GetComponent<ObjectWithNoGravity>();
				if (objectWithNoGravity != null)
					objectWithNoGravity.rigid.AddForce(Physics.gravity);
			}
			else
				rigid.gravityScale = 1;
			for (int i = 0; i < collisionsDict.Count; i ++)
			{
				Collision2D collision = collisionsDict.Values.Get(i);
				ContactPoint2D[] contactPoints2 = new ContactPoint2D[collision.contactCount];
				collision.GetContacts(contactPoints);
				for (int i2 = 0; i2 < contactPoints.Length; i2 ++)
				{
					ContactPoint2D contactPoint = contactPoints[i2];
					for (int i3 = 0; i3 < contactPoints2.Length; i3 ++)
					{
						ContactPoint2D contactPoint2 = contactPoints2[i3];
						if (Vector2.Angle(contactPoint.normal, contactPoint2.normal) >= minCrushAngle)
						{
							Death ();
							return;
						}
					}
				}
			}
			collisionsDict.Add(coll.collider, coll);
		}

		void OnCollisionStay2D (Collision2D coll)
		{
			OnCollisionEnter2D (coll);
		}

		void OnCollisionExit2D (Collision2D coll)
		{
			collisionsDict.Remove(coll.collider);
			rigid.gravityScale = 1;
		}
	}

	public enum RotateMode
	{
		DeltaX,
		DeltaAngle
	}
}