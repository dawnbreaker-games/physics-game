using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class Star : MonoBehaviour
	{
        public static bool playerHasStar;

        void Awake ()
        {
            playerHasStar = false;
        }

		void OnTriggerEnter2D (Collider2D other)
		{
            playerHasStar = true;
            Destroy(gameObject);
		}
	}
}