using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class ActiavtorWall : MonoBehaviour
	{
		// public AudioSource audioSource;
		public UnityEvent onActivate;
		public UnityEvent onDeactivate;
		public float delayAfterActivate;
		public UnityEvent onDelayAfterActivate;
		public _Text timerText;
		public Renderer renderer;
		bool isActivated;
		TimerUpdater timerUpdater;
		
		void OnCollisionEnter2D (Collision2D coll)
		{
			if (isActivated)
				return;
			isActivated = true;
			// audioSource.Play();
			onActivate.Invoke();
			Color color = renderer.material.GetColor("_tint");
			renderer.material.SetColor("_tint", (color / 2).SetAlpha(color.a));
			timerUpdater = new TimerUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(timerUpdater);
		}

		void OnCollisionExit2D (Collision2D coll)
		{
			onDeactivate.Invoke();
			Color color = renderer.material.GetColor("_tint");
			renderer.material.SetColor("_tint", color.SetAlpha(color.a));
			isActivated = false;
			timerText.Text = "";
			GameManager.updatables = GameManager.updatables.Remove(timerUpdater);
		}
		
		void OnDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(timerUpdater);
		}

		class TimerUpdater : IUpdatable
		{
			ActiavtorWall actiavtorWall;
			float timer;

			public TimerUpdater (ActiavtorWall actiavtorWall)
			{
				this.actiavtorWall = actiavtorWall;
				timer = actiavtorWall.delayAfterActivate;
			}

			public void DoUpdate ()
			{
				timer -= Time.deltaTime;
				if (timer <= 0)
				{
					timer = Mathf.Infinity;
					actiavtorWall.renderer.material.SetColor("_tint", Color.white.SetAlpha(actiavtorWall.renderer.material.GetColor("_tint").a));
					actiavtorWall.isActivated = false;
					actiavtorWall.timerText.Text = "";
				}
				else if (timer == Mathf.Infinity)
				{
					if (!actiavtorWall.isActivated)
						actiavtorWall.onDelayAfterActivate.Invoke();
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
				else
					actiavtorWall.timerText.Text = timer.ToString("F1");
			}
		}
	}
}