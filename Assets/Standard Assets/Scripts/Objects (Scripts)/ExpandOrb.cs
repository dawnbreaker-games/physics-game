using Extensions;
using UnityEngine;

namespace PhysicsGame
{
	public class ExpandOrb : MonoBehaviour
	{
		public Transform wallTrs;
		public Rigidbody2D rigid;
		public float expandAmount;
		public float expandDuration;
		public AnimationEntry expandDelayAnimationEntry;
		bool waitingToExpand;
		ExpandUpdater expandUpdater;

		void OnCollisionEnter2D (Collision2D coll)
		{
			if (waitingToExpand)
				return;
			Activator activator = coll.gameObject.GetComponentInParent<Activator>();
			if (activator != null)
			{
				waitingToExpand = true;
				expandDelayAnimationEntry.Play ();
			}
		}

		public void Expand ()
		{
			rigid.bodyType = RigidbodyType2D.Static;
			wallTrs.gameObject.SetActive(true);
			expandUpdater = new ExpandUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(expandUpdater);
		}

		void OnDestroy ()
		{
			if (expandUpdater != null)
				GameManager.updatables = GameManager.updatables.Remove(expandUpdater);
		}

		class ExpandUpdater : IUpdatable
		{
			ExpandOrb expandOrb;

			public ExpandUpdater (ExpandOrb expandOrb)
			{
				this.expandOrb = expandOrb;
			}

			public void DoUpdate ()
			{
				expandOrb.wallTrs.localScale += Vector3.one * expandOrb.expandAmount / expandOrb.expandDuration * Time.deltaTime;
				if (expandOrb.wallTrs.localScale.x >= expandOrb.expandAmount)
				{
					expandOrb.wallTrs.localScale = Vector3.one * expandOrb.expandAmount;
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}