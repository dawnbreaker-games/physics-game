using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class End : MonoBehaviour
	{
		void OnCollisionEnter2D (Collision2D coll)
		{
			if (Star.playerHasStar)
			{
				Star.playerHasStar = false;
				SaveAndLoadManager.saveData.collectedLevelStarsDict[_SceneManager.CurrentScene.name] = true;
				SaveAndLoadManager.Save (SaveAndLoadManager.filePath);
			}
			_SceneManager.instance.LoadScene(_SceneManager.CurrentScene.buildIndex + 1);
		}
	}
}