using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class ShrinkWall : MonoBehaviour
	{
		public Transform trs;
		[HideInInspector]
		public float shrinkRate;
#if UNITY_EDITOR
		public float shrinkDuration;
#endif
		ShrinkUpdater shrinkUpdater;
		uint collisionCount;

#if UNITY_EDITOR
		void OnValidate ()
		{
			shrinkRate = trs.lossyScale.x / shrinkDuration;
		}
#endif

		void OnCollisionEnter2D (Collision2D coll)
		{
			collisionCount ++;
			if (shrinkUpdater != null)
			{
				Rigidbody2D rigid = coll.rigidbody;
				if (rigid == null || rigid.bodyType != RigidbodyType2D.Dynamic)
				{
					GameManager.updatables = GameManager.updatables.Remove(shrinkUpdater);
					return;
				}
			}
			if (collisionCount > 1)
				return;
			shrinkUpdater = new ShrinkUpdater(this);
			GameManager.updatables = GameManager.updatables.Add(shrinkUpdater);
		}

		void OnCollisionExit2D (Collision2D coll)
		{
			collisionCount --;
			if (collisionCount == 0)
				GameManager.updatables = GameManager.updatables.Remove(shrinkUpdater);
		}
		
		void OnDisable ()
		{
			GameManager.updatables = GameManager.updatables.Remove(shrinkUpdater);
		}

		class ShrinkUpdater : IUpdatable
		{
			ShrinkWall shrinkWall;

			public ShrinkUpdater (ShrinkWall shrinkWall)
			{
				this.shrinkWall = shrinkWall;
			}

			public void DoUpdate ()
			{
				shrinkWall.trs.localScale -= Vector3.one * shrinkWall.shrinkRate * Time.deltaTime;
				if (shrinkWall.trs.localScale.x <= 0)
					shrinkWall.gameObject.SetActive(false);
			}
		}
	}
}