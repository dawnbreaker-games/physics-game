using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class Player : SingletonUpdateWhileEnabled<Player>
	{
		public uint id;
		public uint currentPlayerIndex;
		public Avatar[] avatars = new Avatar[0];

		public override void DoUpdate ()
		{
			if (GameManager.paused)
				return;
			if (Keyboard.current.spaceKey.wasPressedThisFrame)
			{
				currentPlayerIndex ++;
				if (currentPlayerIndex >= avatars.Length)
					currentPlayerIndex = 0;
				avatars[currentPlayerIndex].enabled = true;
				Player.instance.enabled = false;
			}
		}
	}
}