using Extensions;
using UnityEngine;
using UnityEngine.Events;
using System.Collections;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class ActivationWall : MonoBehaviour
	{
		// public AudioSource audioSource;
		bool isActivated;
		List<ExpandOrb> touchingOrbs = new List<ExpandOrb>();
		
		void OnCollisionEnter2D (Collision2D coll)
		{
			Activator activator = coll.gameObject.GetComponentInParent<Activator>();
			if (activator == null)
				coll.gameObject.AddComponent<Activator>();
		}
	}
}