#if UNITY_EDITOR
using Extensions;
using UnityEditor;
using UnityEngine;

namespace PhysicsGame
{
	public class DrawTerrain2D : EditorScript
	{
		public _SpriteShapeController spriteShapeController;
		public float pointSeperation;
		Vector2 point = new Vector2();
		Vector2 previousPoint;

		public override void Do ()
		{
			if ((point - previousPoint).sqrMagnitude > pointSeperation * pointSeperation)
			{
				previousPoint = point;
				if (!spriteShapeController.GetPoints().Contains(point))
				{
					Shape2D shape = Shape2D.FromSpline(spriteShapeController.spline);
					int closesetPointIndex = shape.GetClosestCornerIndex(point, true);
					spriteShapeController.spline.InsertPointAt(closesetPointIndex, point);
				}
			}
		}

		public void UpdateHandle ()
		{
			point = Handles.FreeMoveHandle(point, pointSeperation, Vector3.one * spriteShapeController.snapInterval, Handles.CircleHandleCap);
		}
	}

	[CustomEditor(typeof(DrawTerrain2D))]
	public class DrawTerrain2DEditor : EditorScriptEditor
	{
		DrawTerrain2D drawTerrain2D;

		void Awake ()
		{
			drawTerrain2D = (DrawTerrain2D) target;
			if (drawTerrain2D.spriteShapeController == null)
				drawTerrain2D.spriteShapeController = drawTerrain2D.GetComponent<_SpriteShapeController>();
		}

		public override void OnSceneGUI ()
		{
			base.OnSceneGUI ();
			drawTerrain2D.UpdateHandle ();
		}
	}
}
#endif