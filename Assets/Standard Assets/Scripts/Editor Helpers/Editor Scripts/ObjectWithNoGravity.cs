using UnityEngine;

namespace PhysicsGame
{
	[RequireComponent(typeof(Rigidbody2D))]
	[ExecuteInEditMode]
	public class ObjectWithNoGravity : MonoBehaviour
	{
		public Rigidbody2D rigid;
		public Renderer renderer;
		const float LERP_TO_WHITE_AMOUNT = 0.5f;
		const float LINEAR_DRAG = 1;
		const float ANGULAR_DRAG = 5;
		const string ADD_TO_NAME = " (No Gravity)";

#if UNITY_EDITOR
		void OnEnable ()
		{
			if (!Application.isPlaying)
			{
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				if (renderer == null)
					renderer = GetComponent<Renderer>();
			}
			Material material = new Material(renderer.sharedMaterial);
			material.SetFloat("_lerpToWhite", LERP_TO_WHITE_AMOUNT);
			renderer.sharedMaterial = material;
			rigid.gravityScale = 0;
			rigid.linearDamping = LINEAR_DRAG;
			rigid.angularDamping = ANGULAR_DRAG;
			if (!name.Contains(ADD_TO_NAME))
				name += ADD_TO_NAME;
		}

		void OnDisable ()
		{
			Material material = new Material(renderer.sharedMaterial);
			material.SetFloat("_lerpToWhite", 0);
			renderer.sharedMaterial = material;
			rigid.gravityScale = 1;
			rigid.linearDamping = 0;
			rigid.angularDamping = 0;
			name = name.Replace(ADD_TO_NAME, "");
		}
#endif
	}
}