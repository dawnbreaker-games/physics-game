#if UNITY_EDITOR
using UnityEngine;

namespace PhysicsGame
{
	public class MakeCone : EditorScript
	{
        public Cone cone;
        public int basePointCount;
        public Cone.FaceType faceType;
        public float angleToFirstBasePoint;

		public override void Do ()
		{
            cone.MakeMeshRenderer (basePointCount, faceType, angleToFirstBasePoint);
		}
	}
}
#else
namespace PhysicsGame
{
	public class MakeCone : EditorScript
	{
	}
}
#endif