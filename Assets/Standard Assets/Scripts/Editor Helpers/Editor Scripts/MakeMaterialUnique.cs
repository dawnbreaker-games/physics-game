#if UNITY_EDITOR
using System;
using UnityEditor;
using UnityEngine;
using System.Reflection;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class MakeMaterialUnique : EditorScript
	{
		public Renderer renderer;
		
		public override void Do ()
		{
			if (renderer == null)
				renderer = GetComponent<Renderer>();
			renderer.material = new Material(renderer.sharedMaterial);
		}
	}
}
#else
namespace PhysicsGame
{
	public class MakeMaterialUnique : EditorScript
	{
	}
}
#endif
