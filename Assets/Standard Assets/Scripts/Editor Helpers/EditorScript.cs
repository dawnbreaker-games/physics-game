﻿#if UNITY_EDITOR
using System;
using Extensions;
using UnityEngine;
using UnityEditor;
using UnityEngine.Events;

namespace PhysicsGame
{
	[ExecuteInEditMode]
	public class EditorScript : MonoBehaviour
	{
		public static InputEvent inputEvent = new InputEvent();
		public Hotkey[] hotkeys = new Hotkey[0];
		public bool doOnce;
		public bool doRepeatedly;

		public virtual void OnEnable ()
		{
			inputEvent.keys = new KeyCode[0];
			if (Application.isPlaying)
				EditorApplication.update -= Do;
		}

		public virtual void OnDisable ()
		{
			EditorApplication.update -= Do;
		}

		public virtual void OnDestroy ()
		{
			EditorApplication.update -= Do;
		}

		public virtual void OnValidate ()
		{
			if (doRepeatedly)
				EditorApplication.update += Do;
			else
				EditorApplication.update -= Do;
			if (!doOnce)
				return;
			doOnce = false;
			Do ();
		}
		
		public virtual void Do ()
		{
		}

		public virtual void UpdateHotkeys ()
		{
			if (Event.current == null)
				return;
			bool shouldBreak = false;
			inputEvent.mousePosition = Event.current.mousePosition.ToVec2Int();
			inputEvent.type = Event.current.type;
			if (Event.current.type == EventType.KeyDown)
			{
				if (!inputEvent.keys.Contains(Event.current.keyCode))
					inputEvent.keys = inputEvent.keys.Add(Event.current.keyCode);
			}
			else if (Event.current.type == EventType.KeyUp)
				inputEvent.keys = inputEvent.keys.Remove(Event.current.keyCode);
			for (int i = 0; i < hotkeys.Length; i ++)
			{
				Hotkey hotkey = hotkeys[i];
				bool shouldActivateHotkey = true;
				for (int i2 = 0; i2 < hotkey.keys.Length; i2 ++)
				{
					KeyCode key = hotkey.keys[i2];
					if (hotkey.type == Hotkey.Type.ActivateWhileDown && !inputEvent.keys.Contains(key))
					{
						shouldActivateHotkey = false;
						break;
					}
					else if (hotkey.type == Hotkey.Type.ActivateWhileUp && inputEvent.keys.Contains(key))
					{
						shouldActivateHotkey = false;
						break;
					}
				}
				if (shouldActivateHotkey)
					hotkey.action.Invoke();
			}
		}

		public static Vector2Int GetMousePosition ()
		{
			Camera camera = GetSceneViewCamera();
			Vector2Int screenPoint = inputEvent.mousePosition;
			screenPoint.y = camera.pixelHeight - screenPoint.y;
			return screenPoint;
		}

		public static Vector3 GetMousePositionInWorld ()
		{
			return GetSceneViewCamera().ScreenToWorldPoint(GetMousePosition().ToVec2());
		}

		public static Ray GetMouseRay ()
		{
			return GetSceneViewCamera().ScreenPointToRay(GetMousePosition().ToVec3());
		}

		public static Camera GetSceneViewCamera ()
		{
			Camera camera = SceneView.lastActiveSceneView.camera;
			if (camera == null)
				camera = SceneView.currentDrawingSceneView.camera;
			return camera;
		}

		[Serializable]
		public class Hotkey
		{
			public string name;
			public KeyCode[] keys = new KeyCode[0];
			public UnityEvent action;
			public Type type;

			public enum Type
			{
				ActivateOnDown,
				ActivateOnUp,
				ActivateWhileDown,
				ActivateWhileUp
			}
		}

		public class InputEvent
		{
			public Vector2Int mousePosition;
			public EventType type;
			public KeyCode[] keys = new KeyCode[0];
		}
	}

	[CustomEditor(typeof(EditorScript))]
	public class EditorScriptEditor : Editor
	{
		public override void OnInspectorGUI ()
		{
			base.OnInspectorGUI ();
			EditorScript editorScript = (EditorScript) target;
			editorScript.UpdateHotkeys ();
		}

		public virtual void OnSceneGUI ()
		{
			EditorScript editorScript = (EditorScript) target;
			editorScript.UpdateHotkeys ();
		}
	}
}
#else
using UnityEngine;

namespace PhysicsGame
{
	public class EditorScript : MonoBehaviour
	{
		public virtual void Do ()
		{
		}
	}
}
#endif