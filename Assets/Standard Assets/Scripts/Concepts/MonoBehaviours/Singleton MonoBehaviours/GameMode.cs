namespace PhysicsGame
{
	public class GameMode : SingletonMonoBehaviour<GameMode>
	{
		public string displayName;
		public Player playerPrefab;

		public virtual void Begin ()
		{
		}

		public virtual void End ()
		{
			NetworkManager.Disconnect ();
			_SceneManager.instance.RestartScene ();
			// _SceneManager.instance.LoadScene ("Menu");
		}
	}
}