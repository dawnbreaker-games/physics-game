using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class AvatarSelectMenu : Menu, IUpdatable
	{
		public _Text currentAvatarNameText;
		public Transform avatarsParent;
		public Transform[] currentAvatarIndicatorTransforms = new Transform[2];
		public new static AvatarSelectMenu instance;
		public new static AvatarSelectMenu Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<AvatarSelectMenu>(true);
				return instance;
			}
		}
		public static Avatar[] avatars = new Avatar[0];
		public static byte[] currentAvatarIndices = new byte[1];
		public static byte currentAvatarIndicesIndex;
		static int previousSwitchAvatarInput;

		public void Init ()
		{
			avatars = avatarsParent.GetComponentsInChildren<Avatar>();
			for (byte i = 0; i < currentAvatarIndices.Length; i ++)
			{
				SetCurrentAvatarIndicesIndex (i);
				SetCurrentAvatar (avatars[currentAvatarIndices[currentAvatarIndicesIndex]]);
			}
		}

		public override void Open ()
		{
			base.Open ();
			SetCurrentAvatar (avatars[currentAvatarIndices[currentAvatarIndicesIndex]]);
			GameManager.updatables = GameManager.updatables.Add(this);
		}

		public override void Close ()
		{
			gameObject.SetActive(false);
			GameManager.updatables = GameManager.updatables.Remove(this);
			currentAvatarNameText.Text = "";
		}

		public void SetCurrentAvatarIndicesIndex (byte index)
		{
			currentAvatarIndicesIndex = index;
		}

		public void DoUpdate ()
		{
			float movementInput = InputManager.RotateInput;
			if (movementInput != 0 && previousSwitchAvatarInput == 0)
			{
				if (movementInput < 0)
				{
					if (currentAvatarIndices[currentAvatarIndicesIndex] > 0)
						currentAvatarIndices[currentAvatarIndicesIndex] --;
					else
						currentAvatarIndices[currentAvatarIndicesIndex] = (byte) (avatars.Length - 1);
				}
				else
				{
					if (currentAvatarIndices[currentAvatarIndicesIndex] < avatars.Length - 1)
						currentAvatarIndices[currentAvatarIndicesIndex] ++;
					else
						currentAvatarIndices[currentAvatarIndicesIndex] = 0;
				}
				SetCurrentAvatar (avatars[currentAvatarIndices[currentAvatarIndicesIndex]]);
			}
			if (Avatar.instance.unlocked && !EventSystem.current.IsPointerOverGameObject() && (Keyboard.current.enterKey.wasPressedThisFrame || Keyboard.current.spaceKey.wasPressedThisFrame || Mouse.current.leftButton.wasPressedThisFrame))
				Close ();
			previousSwitchAvatarInput = MathfExtensions.Sign(movementInput);
		}

		public void SetCurrentAvatar (Avatar avatar)
		{
			currentAvatarIndicatorTransforms[currentAvatarIndicesIndex].position = avatar.trs.position;
			string text = avatar.displayName;
			if (avatar.unlockOnCompleteAchievement != null)
			{
				text += "\n";
				if (avatar.unlockOnCompleteAchievement.complete)
					text += "(Done) ";
				text += avatar.unlockOnCompleteAchievement.displayName;
			}
			currentAvatarNameText.Text = text;
		}
	}
}