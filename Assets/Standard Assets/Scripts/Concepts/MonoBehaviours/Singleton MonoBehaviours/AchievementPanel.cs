using Extensions;
using UnityEngine;

namespace PhysicsGame
{
	public class AchievementPanel : SingletonMonoBehaviour<AchievementPanel>
	{
		public Transform achievementIndicatorsParent;
		public AchievementIndicator achievementIndicatorPrefab;

		public void DoUpdate ()
		{
			GameManager.instance.DestroyChildren (achievementIndicatorsParent);
			Achievement[] achievements = FindObjectsOfType<Achievement>();
			for (int i = 0; i < achievements.Length; i ++)
			{
				Achievement achievement = achievements[i];
				AchievementIndicator achievementIndicator = Instantiate(achievementIndicatorPrefab, achievementIndicatorsParent);
				achievementIndicator.trs.localScale = Vector3.one;
				string text = achievement.displayName;
				if (achievement.complete)
					text = " (Done) " + text;
				if (!achievement.complete)
					text += " (" + achievement.GetProgress() + "/" + achievement.GetMaxProgress() + ")";
				achievementIndicator.text.Text = text;
			}
		}
	}
}