using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class GameCamera : CameraScript
	{
        public new static GameCamera instance;
		public new static GameCamera Instance
		{
			get
			{
				if (instance == null)
					instance = Camera.main.GetComponent<GameCamera>();
				return instance;
			}
		}
		
		public override void Awake ()
		{
			base.Awake ();
			trs.SetParent(null);
		}

		public override void HandlePosition ()
		{
            base.HandlePosition ();
            trs.position = Avatar.Instance.trs.position.SetZ(trs.position.z);
		}
	}
}