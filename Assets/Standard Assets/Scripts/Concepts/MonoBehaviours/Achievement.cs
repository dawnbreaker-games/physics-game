using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class Achievement : MonoBehaviour
	{
		public string displayName;
		public GameObject incompleteIndicatorGo;
		[HideInInspector]
		public bool complete;
		public static Achievement[] instances = new Achievement[0];

		public void Init ()
		{
			if (SaveAndLoadManager.saveData.completeAchievementsNames.Contains(name))
				Complete ();
		}

		public bool ShouldBeComplete ()
		{
			return GetProgress() >= GetMaxProgress();
		}
		
		public virtual void Complete ()
		{
			if (complete)
				return;
			incompleteIndicatorGo.SetActive(false);
			if (!SaveAndLoadManager.saveData.completeAchievementsNames.Contains(name))
				SaveAndLoadManager.saveData.completeAchievementsNames = SaveAndLoadManager.saveData.completeAchievementsNames.Add(name);
			complete = true;
			print(name + " complete");
		}

		public virtual uint GetProgress ()
		{
			throw new NotImplementedException();
		}

		public virtual uint GetMaxProgress ()
		{
			throw new NotImplementedException();
		}
	}
}