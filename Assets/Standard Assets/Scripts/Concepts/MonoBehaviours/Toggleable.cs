using Extensions;
using UnityEngine;
using UnityEngine.U2D;
using System.Collections;
using System.Collections.Generic;

namespace PhysicsGame
{
	[ExecuteInEditMode]
	public class Toggleable : MonoBehaviour
	{
		[HideInInspector]
		public bool initActive;
		public bool active;
		public Renderer renderer;
		public Collider2D collider;
		public ToggleBehaviour toggleBehaviour;
		public static Toggleable[] instances = new Toggleable[0];
		Toggleable[] toggleChildren = new Toggleable[0];

		void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (renderer == null)
					renderer = GetComponent<Renderer>();
				if (collider == null)
					collider = GetComponent<Collider2D>();
				return;
			}
#endif
			toggleChildren = GetComponentsInChildren<Toggleable>();
			for (int i = 1; i < toggleChildren.Length; i ++)
			{
				Toggleable toggle = toggleChildren[i];
				toggle.initActive = active;
				if (toggle.active != active)
					toggle.Toggle ();
			}
		}

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (Application.isPlaying)
				return;
			initActive = active;
			if (collider != null && collider.enabled != active)
			{
				active = !active;
				Toggle ();
			}
		}
#endif

		public void Toggle ()
		{
			active = !active;
			for (int i = 1; i < toggleChildren.Length; i ++)
			{
				Toggleable toggle = toggleChildren[i];
				if (toggle.active != active)
					toggle.Toggle ();
			}
			if (toggleBehaviour != ToggleBehaviour.OnlyToggleChildren)
			{
				collider.enabled = active;
				if (active)
				{
					if (toggleBehaviour == ToggleBehaviour.RendererColor)
					{
						SpriteRenderer spriteRenderer = renderer as SpriteRenderer;
						if (spriteRenderer != null)
							spriteRenderer.color = spriteRenderer.color.MultiplyAlpha(4);
						else
						{
							SpriteShapeRenderer spriteShapeRenderer = renderer as SpriteShapeRenderer;
							if (spriteShapeRenderer != null)
								spriteShapeRenderer.color = spriteShapeRenderer.color.MultiplyAlpha(4);
						}
					}
					else// if (toggleBehaviour == ToggleBehaviour.MaterialTint)
					{
						Material material = new Material(renderer.sharedMaterial);
						material.SetColor("_tint", material.GetColor("_tint").MultiplyAlpha(4));
						renderer.sharedMaterial = material;
					}
				}
				else
				{
					if (toggleBehaviour == ToggleBehaviour.RendererColor)
					{
						SpriteRenderer spriteRenderer = renderer as SpriteRenderer;
						if (spriteRenderer != null)
							spriteRenderer.color = spriteRenderer.color.DivideAlpha(4);
						else
						{
							SpriteShapeRenderer spriteShapeRenderer = renderer as SpriteShapeRenderer;
							if (spriteShapeRenderer != null)
								spriteShapeRenderer.color = spriteShapeRenderer.color.DivideAlpha(4);
						}
					}
					else// if (toggleBehaviour == ToggleBehaviour.MaterialTint)
					{
						Material material = new Material(renderer.sharedMaterial);
						material.SetColor("_tint", material.GetColor("_tint").DivideAlpha(4));
						renderer.sharedMaterial = material;
					}
				}
			}
		}

		public enum ToggleBehaviour
		{
			RendererColor,
			MaterialTint,
			OnlyToggleChildren
		}
	}
}