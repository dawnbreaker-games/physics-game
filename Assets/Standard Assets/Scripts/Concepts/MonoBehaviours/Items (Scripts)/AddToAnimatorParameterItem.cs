using Extensions;
using UnityEngine;

namespace PhysicsGame
{
	public class AddToAnimatorParameterItem : Item
	{
		public string parameterName;
		public float amount;

		public override void OnGain ()
		{
			Animator animator = owner.animator;
			animator.SetFloat(parameterName, animator.GetFloat(parameterName) + amount);
		}
	}
}