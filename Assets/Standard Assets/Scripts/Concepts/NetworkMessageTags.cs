namespace PhysicsGame
{
    public class NetworkMessageTags
    {
        public const int SPAWN_PLAYER = 0;
        public const int BEGIN_GAME = 1;
        public const int AVATAR_MOVED = 2;
        public const int PLAYER_LEFT = 3;
    }
}