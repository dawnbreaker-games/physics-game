using System;
using DarkRift;
using System.Net;
using Extensions;
using UnityEngine;
using PlayerIOClient;
using DarkRift.Client;
using System.Net.Sockets;
using System.Collections;
using System.Diagnostics;
using DarkRift.Client.Unity;
using System.Collections.Generic;
using Message = DarkRift.Message;

namespace PhysicsGame
{
	public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
	{
		public UnityClient darkRift2Client;
		// public TMP_Text avatarListText;
		// public TMP_Text currentAvatarText;
		// public NotificationText notificationTextPrefab;
		// public Transform notificationTextsParent;
		public static Dictionary<uint, Player> playersDict = new Dictionary<uint, Player>();
		public static Connection connection;
		public static Client playerIOClient;
		static DatabaseObject dbObj;

		void Start ()
		{
			Connect_DarkRift2 ();
		}

		public static void Connect_PlayerIO (Callback<Client> onSuccess, Callback<PlayerIOError> onFail)
		{
			PlayerIO.UseSecureApiRequests = true;
			PlayerIO.Authenticate("4-way-tag-mm877zzttuozyme7bmeslw",
				"public",
				new Dictionary<string, string> {
					{ "userId", LocalUserInfo.username },
				},
				null,
				onSuccess,
				onFail
			);
		}

		public void Connect_DarkRift2 ()
		{
			darkRift2Client.MessageReceived -= OnMessageReceived;
			darkRift2Client.MessageReceived += OnMessageReceived;
			darkRift2Client.ConnectInBackground(GetLocalIPAddress(), darkRift2Client.Port, false, OnConnectDone);
		}

		void OnConnectDone (Exception e)
		{
			if (e != null)
			{
				print(e.Message + "\n" + e.StackTrace);
				Connect_DarkRift2 ();
			}
			else
			{
				DarkRiftWriter writer = DarkRiftWriter.Create();
				writer.Write(AvatarSelectMenu.currentAvatarIndices[0]);
				Message message = Message.Create(NetworkMessageTags.BEGIN_GAME, writer);
				darkRift2Client.SendMessage(message, SendMode.Reliable);
			}
		}

		void OnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				if (message.Tag == NetworkMessageTags.SPAWN_PLAYER)
					OnSpawnPlayer (eventArgs);
				else if (message.Tag == NetworkMessageTags.AVATAR_MOVED)
					OnAvatarMoved (eventArgs);
				else if (message.Tag == NetworkMessageTags.PLAYER_LEFT)
					OnPlayerLeft (eventArgs);
			}
		}

		void OnSpawnPlayer (MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						byte avatar1Index = reader.ReadByte();
						Vector2 avatarPosition = new Vector2(reader.ReadSingle(), reader.ReadSingle());
						if (!playersDict.ContainsKey(id))
							SpawnPlayer (avatarPosition, id, avatar1Index);
					}
				}
			}
		}

		void OnAvatarMoved (MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						bool isFirstAvatar = reader.ReadBoolean();
						Vector2 position = new Vector2(reader.ReadSingle(), reader.ReadSingle());
						Player player;
						if (playersDict.TryGetValue(id, out player))
							player.avatars[isFirstAvatar.GetHashCode()].trs.position = position;
					}
				}
			}
		}

		void OnPlayerLeft (MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage())
			{
				using (DarkRiftReader reader = message.GetReader())
				{
					while (reader.Position < reader.Length)
					{
						uint id = reader.ReadUInt32();
						Player player;
						if (playersDict.TryGetValue(id, out player))
						{
							for (int i = 0; i < player.avatars.Length; i ++)
							{
								Avatar avatar = player.avatars[i];
								Destroy(avatar.gameObject);
							}
							// NotificationText notificationText = ObjectPool.instance.SpawnComponent<NotificationText>(notificationTextPrefab.prefabIndex, parent:notificationTextsParent);
							// notificationText.text.text = "Avatar " + id + " left";
							playersDict.Remove(id);
							// UpdateAvatarListText ();
						}
					}
				}
			}
		}

		public Player SpawnPlayer (Vector2 avatarPosition, uint id, byte avatarIndex)
		{
			Player output = Instantiate(GameMode.instance.playerPrefab);
			Avatar avatar = Instantiate(AvatarSelectMenu.avatars[avatarIndex], avatarPosition, Quaternion.identity);
			output.avatars = new Avatar[] { avatar };
			output.id = id;
			// NotificationText notificationText = ObjectPool.instance.SpawnComponent<NotificationText>(notificationTextPrefab.prefabIndex, parent:notificationTextsParent);
			// notificationText.text.text = "Avatar " + id + " joined";
			playersDict.Add(id, output);
			// UpdateAvatarListText ();
			if (id == darkRift2Client.ID)
			// {
				avatar.enabled = true;
			// 	currentAvatarText.text = "You are: " + id;
			// }
			// entitySpatialHashGridAgentsDict.Add(output, new SpatialHashGrid2D<Entity>.Agent(position.GetXZ(), output, entitySpatialHashGrid));
			return output;
		}

		// void UpdateAvatarListText ()
		// {
		// 	avatarListText.text = "Avatars: ";
		// 	foreach (KeyValuePair<uint, Avatar> keyValuePair in playersDict)
		// 		avatarListText.text += keyValuePair.Key + ", ";
		// }

		public static void Disconnect ()
		{
			if (connection != null)
				connection.Disconnect();
			if (playerIOClient != null)
				playerIOClient.Logout();
		}

		public static void DisplayError (PlayerIOError error)
		{
			GameManager.instance.DisplayNotification (error.ToString());
		}

		public static void DisplayErrorAndRetry (PlayerIOError error)
		{
			DisplayError (error);
			new StackTrace().GetFrame(1).GetMethod().Invoke(null, null);
		}

		public static void PrintError (PlayerIOError error)
		{
			print(error.ToString());
		}
		
		public static string GetLocalIPAddress ()
		{
			var host = Dns.GetHostEntry(Dns.GetHostName());
			foreach (var ip in host.AddressList)
			{
				if (ip.AddressFamily == AddressFamily.InterNetwork)
					return ip.ToString();
			}
			throw new System.Exception("No network adapters with an IPv4 address in the system!");
		}
	}
}