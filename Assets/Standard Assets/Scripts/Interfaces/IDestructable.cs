﻿namespace PhysicsGame
{
	public interface IDestructable
	{
		float Hp { get; set; }
		int MaxHp { get; set; }
		
		void TakeDamage (float amount);
		void Death ();
	}
}