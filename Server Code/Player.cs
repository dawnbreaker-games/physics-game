public class Player
{
    public uint id;
    public Avatar[] avatars = new Avatar[1];

    public Player (uint id, Avatar[] avatars)
    {
        this.id = id;
        this.avatars = avatars;
    }
}