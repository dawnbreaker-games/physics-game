using System;
using DarkRift;
using DarkRift.Server;
using System.Collections;
using System.Collections.Generic;

namespace PhysicsGame
{
	public class PhysicsGamePlugin : Plugin
	{
		public override Version Version => new Version(1, 0, 0);
		public override bool ThreadSafe => true;
		public static PhysicsGamePlugin instance;
		public static Vector2[] spawnPoints = new Vector2[] { new Vector2() };
		static Random random = new Random();
		static Dictionary<uint, Player> playersDict = new Dictionary<uint, Player>();

		public PhysicsGamePlugin (PluginLoadData pluginLoadData) : base (pluginLoadData)
		{
			instance = this;
			ClientManager.ClientConnected += OnClientConnected;
			ClientManager.ClientDisconnected += OnClientDisconnected;
		}

		void OnClientConnected (object sender, ClientConnectedEventArgs eventArgs)
		{
			foreach (Player previousPlayer in playersDict.Values)
			{
				using (DarkRiftWriter writer = DarkRiftWriter.Create())
				{
					writer.Write(previousPlayer.id);
					writer.Write(previousPlayer.avatars[0].index);
					writer.Write(previousPlayer.avatars[0].position.x);
					writer.Write(previousPlayer.avatars[0].position.y);
					using (Message message = Message.Create(NetworkMessageTags.SPAWN_PLAYER, writer))
						eventArgs.Client.SendMessage(message, SendMode.Reliable);
				}
			}
			eventArgs.Client.MessageReceived += OnBeginGameMessageReceived;
		}

		void OnClientDisconnected (object sender, ClientDisconnectedEventArgs eventArgs)
		{
			using (DarkRiftWriter writer = DarkRiftWriter.Create())
			{
				writer.Write(eventArgs.Client.ID);
				using (Message message = Message.Create(NetworkMessageTags.PLAYER_LEFT, writer))
				{
					IClient[] clients = ClientManager.GetAllClients();
					for (int i = 0; i < clients.Length; i ++)
					{
						IClient client = clients[i];
						if (client != eventArgs.Client)
							client.SendMessage(message, SendMode.Reliable);
					}
				}
			}
			playersDict.Remove(eventArgs.Client.ID);
			eventArgs.Client.MessageReceived -= OnMessageReceived;
			eventArgs.Client.MessageReceived -= OnBeginGameMessageReceived;
		}

		void OnMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			using (Message message = eventArgs.GetMessage() as Message)
			{
				if (message.Tag == NetworkMessageTags.AVATAR_MOVED)
					OnAvatarMoved (eventArgs);
			}
		}

		void OnBeginGameMessageReceived (object sender, MessageReceivedEventArgs eventArgs)
		{
			OnBeginGame (eventArgs);
			eventArgs.Client.MessageReceived -= OnBeginGameMessageReceived;
			eventArgs.Client.MessageReceived += OnMessageReceived;
		}

		void OnBeginGame (MessageReceivedEventArgs eventArgs)
		{
			DarkRiftReader reader = eventArgs.GetMessage().GetReader();
			byte avatar1Index = reader.ReadByte();
			Vector2 avatar1SpawnPoint = spawnPoints[random.Next(0, spawnPoints.Length)];
			Avatar avatar1 = new Avatar(avatar1Index, avatar1SpawnPoint);
			Player player = new Player(eventArgs.Client.ID, new Avatar[] { avatar1 });
			DarkRiftWriter writer = DarkRiftWriter.Create();
			writer.Write(player.id);
			writer.Write(avatar1Index);
			writer.Write(player.avatars[0].position.x);
			writer.Write(player.avatars[0].position.y);
			Message message = Message.Create(NetworkMessageTags.SPAWN_PLAYER, writer);
			IClient[] clients = ClientManager.GetAllClients();
			for (int i = 0; i < clients.Length; i ++)
			{
				IClient client = clients[i];
				client.SendMessage(message, SendMode.Reliable);
			}
			playersDict.Add(player.id, player);
		}

		void OnAvatarMoved (MessageReceivedEventArgs eventArgs)
		{
			Message message = eventArgs.GetMessage();
			DarkRiftReader reader = message.GetReader();
			Player player = playersDict[reader.ReadUInt32()];
			player.avatars[0].position = new Vector2(reader.ReadSingle(), reader.ReadSingle());
			IClient[] clients = ClientManager.GetAllClients();
			for (int i = 0; i < clients.Length; i ++)
			{
				IClient client = clients[i];
				if (client != eventArgs.Client)
					client.SendMessage(message, eventArgs.SendMode);
			}
		}
	}
}